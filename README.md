# Python est lent ! Comment l'accélérer jusqu'à 453 millions d'opérations par seconde ? Apprendre Cython Numba PyPy maintenant 

<h2 align="right">Les sources et le code </h2>



<p align="right">
Python est long!? est ce que peut-on le rendre plus rapide 🤫 voici le secret:
* Comment les performances sont-elles affectées ?
* Que se passe-t-il dans les coulisses ?
* Comment fonctionne Python ?
* Quelle est la nature de la relation entre celui-ci et le langage C ? 
  
Sur le chemin parcouru par votre code pour devenir un langage machine et s'adresser à des processeurs informatiques 
En répondant à ces questions, vous en apprendrez davantage sur le compilateur et l'interpréteur et sur le chemin parcouru par votre code pour devenir un langage machine et s'adresser aux processeurs informatiques, en plus du typage dynamique (reconnaissance automatique des données) et du multitraitement (multi-traitement), jusqu'à ce que nous rendions le langage Python des milliers de fois plus rapide en utilisant l'une de ces bibliothèques :

* Cython
* Numba
* PyPy

Et croyez-moi si je vous dis que ce tutoriel sera convivial pour les débutants !</p>
 
# Code Python utilisé dans l'explication
### Il est préférable de télécharger les codes ci-dessus dans le référentiel au lieu de les copier-coller, car les noms de fichiers sont importants.
## Vanilla Python


```python
def prime_py(range_start, range_end):
	count_of_primes = 0
	range_start = range_start if range_start >= 2 else 2
	for num in prange(range_start, range_end + 1):
		for div_num in prange(2, num):
			if ((num % div_num) == 0):
				break
		else:
			count_of_primes += 1

	return count_of_primes
```
# Cython
### Installation Cython

<!-- <h2 >Install cython </h2> -->
<p align="right">
Début de l'installation du package cython :
</p>

## Linux/Ubuntu

```bash
pip3 install cython
```
Outils de compilation C/C++ (essentiel)
```bash
sudo apt-get install gcc
sudo apt-get install build-essential
```
## Mac OS
Téléchargez les outils de développement XCode via
Installer le module complémentaire Outils de ligne de commande
Vous devez d'abord installer xcode, puis accepter d'installer les outils de ligne de commande.

```bash
sudo xcode-select --install
pip3 install cython
```
## Windows

```cmd
python -m pip install cython
```

Vous devez installer MinGW qui inclut les compilateurs C d'ici [SourceForge](https://sourceforge.net/projects/mingw/)

L'étape suivante est très nécessaire, vous devez ajouter le chemin du dossier Bin du programme aux chemins d'environnement dans le système Windows, c'est facile, allez dans Démarrer et tapez Variables d'environnement , puis cliquez sur Variables d'environnement, recherchez la variable PATH, puis ajoutez-y un chemin de dossier Bin et enregistrez.

L'important est qu'après avoir installé le compilateur du langage C, quel que soit votre système d'exploitation, assurez-vous qu'il fonctionne en tapant la commande suivante :

```bash
gcc –version
```
Si vous voyez la version du traducteur, vous êtes prêt.

<h2 >Cython .Pyx Code</h2>

```python
def prime_py(int range_start, int range_end):
    cdef int count_of_primes = 0
    cdef int num
    cdef int div_num
	range_start = range_start if range_start >=  2  else  2
	for num in prange(range_start, range_end +  1):
		for div_num in prange(2, num):
			if ((num % div_num) ==  0):
				break
		else:
			count_of_primes +=  1

	return count_of_primes
```

<h2 >Cython import Code</h2>

```python
import pyximport; pyximport.install()

from Primes_cython import prime_py

print(prime_py(0,100000))
```
* Attention : Le fichier Primes_cython doit être dans le même dossier de code 
# Numba 
### Installation Numba

```bash
pip install numba
```

[documentation officielle de numba](https://numba.pydata.org/)
Numba traduit les fonctions écrites en Python en langage machine lors de l'exécution, à l'aide d'un compilateur appelé LLVM.
Ce compilateur récupère les fonctions du bytecode avant qu'elles ne passent à l'interpréteur Python et les convertit en code machine natif pour les exécuter directement sur le processeur.

---
Et la vitesse de ce compilateur avec Namba peut être aussi rapide que C et Fortran, contrairement à d'autres solutions , vous n'avez pas besoin de remplacer le compilateur Python ou d'exécuter une étape de compilation séparée ou même d'installer le compilateur C, installez simplement la bibliothèque et ajoutez le Décorateur à votre code, Namba fait le reste !
Rien de plus simple, suivez l'expérience et les explications dans la vidéo sur YouTube.


```python
from numba import jit

@jit()
def  prime_py(range_start, range_end):
	count_of_primes =  0
	range_start = range_start if range_start >=  2  else  2
	for num in prange(range_start, range_end +  1):
		for div_num in prange(2, num):
			if ((num % div_num) ==  0):
				break
		else:
			count_of_primes +=  1

	return count_of_primes
```
<h2>Numba Prange </h2>


```python
from numba import jit , prange

@jit(parallel=True)
def  prime_py(range_start, range_end):
	count_of_primes =  0
	range_start = range_start if range_start >=  2  else  2
	for num in prange(range_start, range_end +  1):
		for div_num in prange(2, num):
			if ((num % div_num) ==  0):
				break
		else:
			count_of_primes +=  1

	return count_of_primes
```

# PyPy
###  Installation PyPy

Donc, comme il est connu que PyPy peut être considéré comme une distribution différente de Python, nous le téléchargeons à partir de
[Télécharger depuis leur site officiel](https://www.pypy.org/)

 Nous décompressons et le mettons dans un dossier quelque part sur notre ordinateur, je préfère le lecteur C
Accédez au chemin de l'environnement et ajoutez-y le chemin du répertoire
Alors maintenant, si nous tapons pypy dans la ligne de commande, cela devrait fonctionner comme Python

